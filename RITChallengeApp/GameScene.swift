//
//  GameScene.swift
//  RITChallengeApp
//
//  Created by Gabriel Ortega on 2/5/15.
//  Copyright (c) 2015 Gabriel Ortega. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var spaceShips:Array<SKNode> = Array()
    let platform:SKShapeNode = SKShapeNode(rectOfSize: CGSizeMake(20, UIScreen.mainScreen().bounds.width))
    let ball:SKShapeNode = SKShapeNode(circleOfRadius: 20)
    
    
    func applyRotation(rotation:Double) {
        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
            self.platform.zRotation = CGFloat(rotation)
        }
    }
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        
        self.physicsWorld.gravity = CGVectorMake(0, -9.8);
        
        platform.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        platform.fillColor = UIColor.yellowColor()
        platform.physicsBody = SKPhysicsBody(rectangleOfSize:platform.frame.size)
        platform.physicsBody!.affectedByGravity = false
        platform.physicsBody!.dynamic = false
        platform.zRotation = 45.0
        self.addChild(platform)
        
        ball.physicsBody = SKPhysicsBody(circleOfRadius: 20)
        ball.physicsBody!.affectedByGravity = true
        ball.fillColor = UIColor.redColor()
        ball.position = CGPoint(x: CGRectGetMidX(self.frame), y: 800)
        ball.physicsBody!.restitution = 1.0
        self.addChild(ball)
        self.physicsWorld.gravity = CGVectorMake(0, -9.8);

        
        
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */

        
//        for touch: AnyObject in touches {
//            let location = touch.locationInNode(self)
//            
//            ball.position = location
//            
//            
//            if let sprite = spaceShips.first {
//                sprite.position = location
//                
//                var action:SKAction = SKAction.rotateByAngle((CGFloat) (M_PI), duration: 0.5)
//                SKAction.repeatActionForever(action)
//                sprite.runAction(action)
//                sprite.physicsBody!.affectedByGravity = true;
//                spaceShips.removeAtIndex(0)
//                self.addChild(sprite)
//                NSLog("ADDING SPRITE")
//
//            }
//        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        
        for sprite in self.children as [SKNode] {
            if (sprite.position.y < -CGRectGetHeight(sprite.frame)) {
                sprite.position = CGPointMake(CGRectGetMidX(self.frame), -300.0)
                sprite.physicsBody!.affectedByGravity = false
                sprite.removeFromParent()
                self.spaceShips.append(sprite)
                NSLog("REMOVING SPRITE")

            }
        }
    }
}
