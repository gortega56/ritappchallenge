//
//  GameViewController.swift
//  RITChallengeApp
//
//  Created by Gabriel Ortega on 2/5/15.
//  Copyright (c) 2015 Gabriel Ortega. All rights reserved.
//

import UIKit
import SpriteKit
import CoreMotion

extension SKNode {
    class func unarchiveFromFile(file : NSString) -> SKNode? {
        if let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks") {
            var sceneData = NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe, error: nil)!
            var archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
            
            archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
            let scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as GameScene
            archiver.finishDecoding()
            return scene
        } else {
            return nil
        }
    }
}

class GameViewController: UIViewController {

    
    var cm:CMMotionManager = CMMotionManager()
    var scene:GameScene? = nil
    var min = 0.0
    var max = 0.0
    
    override func viewWillLayoutSubviews() {
        let value = UIInterfaceOrientation.LandscapeLeft.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let scene = GameScene.unarchiveFromFile("GameScene") as? GameScene {
            // Configure the view.
            let skView = self.view as SKView
            skView.showsFPS = true
            skView.showsNodeCount = true
            skView.showsPhysics = true
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = SKSceneScaleMode.AspectFill

            self.scene = scene
            skView.presentScene(scene)
        }
        
        var tick = 0.1
        
        if (cm.deviceMotionAvailable == true) {
            cm.deviceMotionUpdateInterval = tick
            cm.startDeviceMotionUpdatesToQueue(NSOperationQueue(), withHandler:  { (deviceMotion:CMDeviceMotion!, error:NSError!) -> Void in
                let rotation = atan2(deviceMotion.gravity.x, deviceMotion.gravity.y) - M_PI
                self.scene!.applyRotation(rotation)
            })
        }
    }

    override func shouldAutorotate() -> Bool {
        return false
    }

    override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.Landscape.rawValue)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
